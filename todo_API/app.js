const express = require("express");
const app = express();
const fs = require("fs");
const shortid = require("shortid");
const lists = require("./lists.json");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// get all list data
app.get("/lists", (req, res) => {
  return res.send({ listData: lists });
});

// add new list
app.post("/lists", (req, res) => {
  console.log(req.body);
  const { listName } = req.body;
  lists.push({
    listId: "L" + shortid.generate(),
    listName: listName,
    listItems: [],
  });

  const jLists = JSON.stringify(lists, null, 2);
  fs.writeFile("lists.json", jLists, (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });

  return res.send({ listData: lists });
});

// add new item to list
app.post("/lists/:listid", (req, res) => {
  const { listid } = req.params;
  const { newItemName } = req.body;

  let changedListItems;
  const updatedLists = lists.map((list) => {
    if (list.listId == listid) {
      list.listItems.push({
        itemId: "I" + shortid.generate(),
        name: newItemName,
        completed: false,
      });
      changedListItems = list.listItems;
    }
    return list;
  });

  const jLists = JSON.stringify(updatedLists, null, 2);
  fs.writeFile("lists.json", jLists, (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });

  return res.send({ listItems: changedListItems });
});

// edit item (either name or completed status)
app.patch("/lists/:listid/:itemid", (req, res) => {
  const { listid, itemid } = req.params;
  const { itemName, itemCompleted } = req.body;

  let changedListItems;

  const updatedLists = lists.map((list) => {
    if (list.listId == listid) {
      list.listItems = list.listItems.map((item) => {
        if (itemName && item.itemId == itemid) {
          item.name = itemName;
        }
        if (typeof itemCompleted !== "undefined" && item.itemId == itemid) {
          item.completed = itemCompleted;
        }
        return item;
      });
      changedListItems = list.listItems;
    }
    return list;
  });

  const jLists = JSON.stringify(updatedLists, null, 2);
  fs.writeFile("lists.json", jLists, (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });

  return res.send({ listItems: changedListItems });
});

// delete list
app.delete("/lists/:listid", (req, res) => {
  const { listid } = req.params;
  const updatedLists = lists.filter((list) => list.listId != listid);

  const jLists = JSON.stringify(updatedLists, null, 2);
  fs.writeFile("lists.json", jLists, (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });
  return res.send({ listData: updatedLists });
});

// delete item
app.delete("/lists/:listid/:itemid", (req, res) => {
  const { listid, itemid } = req.params;
  let changedListItems;

  const updatedLists = lists.map((list) => {
    if (list.listId == listid) {
      list.listItems = list.listItems.filter((item) => item.itemId != itemid);
      changedListItems = list.listItems;
    }
    return list;
  });

  const jLists = JSON.stringify(updatedLists, null, 2);
  fs.writeFile("lists.json", jLists, (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });
  return res.send({ listItems: changedListItems });
});

const port = 80;

app.listen(port, (error) => {
  if (error) {
    console.log("error");
  }
  console.log("server running on port", port);
});
