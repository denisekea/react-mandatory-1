import React, { Component } from "react";
import styled from "styled-components";
import TodoItem from "./TodoItem";
import NewItemForm from "./NewItemForm";
import ListHeader from "./ListHeader";

const ListContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 2.5em auto auto 2.2em;
  row-gap: 1em;
  background-color: white;
  border-radius: 10px;
  padding: 1em;
  height: auto;
`;

export default class TodoList extends Component {
  state = {
    listItems: this.props.list.listItems,
  };

  handleUpdateListItems = (listItems) => {
    this.setState({ listItems });
  };

  render() {
    const { listItems } = this.state;
    const { list, setLists } = this.props;

    return (
      <ListContainer>
        <ListHeader list={list} setLists={setLists} />
        <div>
          {listItems.length
            ? listItems
                .filter((item) => !item.completed)
                .map((item) => {
                  return (
                    <TodoItem
                      key={item.itemId}
                      listId={list.listId}
                      item={item}
                      setListItems={this.handleUpdateListItems}
                    />
                  );
                })
            : "Nothing to do"}
        </div>
        <div style={{ borderTop: "1px solid black", paddingTop: "1em" }}>
          {listItems
            .filter((item) => item.completed)
            .map((item) => {
              return (
                <TodoItem
                  key={item.itemId}
                  listId={list.listId}
                  item={item}
                  setListItems={this.handleUpdateListItems}
                />
              );
            })}
        </div>
        <NewItemForm
          listId={list.listId}
          setListItems={this.handleUpdateListItems}
        />
      </ListContainer>
    );
  }
}
