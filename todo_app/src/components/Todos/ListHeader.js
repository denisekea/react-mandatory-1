import React, { Component } from "react";
import styled from "styled-components";
import { FiTrash2 as DeleteIcon } from "react-icons/fi";

const ListHeaderContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1.5em;
  column-gap: 5px;
  align-items: center;

  h2 {
    display: inline-block;
    font-family: "Montserrat", sans-serif;
    margin: 0;
    font-size: 1.4em;
    font-weight: 700;
  }

  svg {
    cursor: pointer;
    stroke: lightgrey;
    height: 17px;
    width: 17px;

    &:hover {
      stroke: red;
    }
  }
`;

export default class ListHeader extends Component {
  handleDeleteList = async () => {
    const { list, setLists } = this.props;

    const connection = await fetch(`/lists/${list.listId}`, {
      method: "DELETE",
    });
    const data = await connection.json();
    setLists(data.listData);
  };

  render() {
    const { list } = this.props;
    return (
      <ListHeaderContainer>
        <h2>{list.listName}</h2>
        <DeleteIcon onClick={this.handleDeleteList} />
      </ListHeaderContainer>
    );
  }
}
