import React, { Component } from "react";
import styled from "styled-components";
import {
  FiCircle as UncheckedIcon,
  FiCheckCircle as CheckedIcon,
  FiTrash as DeleteIcon,
} from "react-icons/fi";

export const ItemContainer = styled("div")`
  display: grid;
  grid-template-columns: 1.5em 1fr 1.5em;
  align-items: center;
  margin: 5px 0;

  .completedText {
    text-decoration: line-through;
  }

  svg {
    cursor: pointer;

    &:hover {
      fill: #69f6b7;
    }

    &.deleteIcon {
      fill: none;
      &:hover {
        stroke: red;
      }
    }
  }
`;

export default class TodoItem extends Component {
  handleCompletedStatus = async () => {
    const { listId, item, setListItems } = this.props;

    const connection = await fetch(`/lists/${listId}/${item.itemId}`, {
      method: "PATCH",
      body: JSON.stringify({ itemCompleted: !item.completed }),
      headers: { "Content-Type": "application/json" },
    });
    const data = await connection.json();
    setListItems(data.listItems); // update list items
  };

  handleDeleteItem = async () => {
    const { listId, item, setListItems } = this.props;

    const connection = await fetch(`/lists/${listId}/${item.itemId}`, {
      method: "DELETE",
    });
    const data = await connection.json();
    setListItems(data.listItems);
  };

  render() {
    const {
      item: { completed, name },
    } = this.props;

    return (
      <ItemContainer>
        {completed ? (
          <CheckedIcon onClick={this.handleCompletedStatus} />
        ) : (
          <UncheckedIcon onClick={this.handleCompletedStatus} />
        )}
        <span className={completed ? "completedText" : ""}>{name}</span>
        <DeleteIcon className="deleteIcon" onClick={this.handleDeleteItem} />
      </ItemContainer>
    );
  }
}
