import React, { Component } from "react";
import styled from "styled-components";
import { FiPlus as PlusIcon } from "react-icons/fi";

export const FormContainer = styled("div")`
  width: 100%;
  padding-top: 5px;

  form {
    display: grid;
    grid-template-columns: 1fr 30px;
    column-gap: 0.3em;
  }

  input {
    font-family: "Raleway", sans-serif;
    box-sizing: border-box;
    height: 30px;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    font-size: 0.9em;
    background-color: #efeafd;
  }

  input:focus {
    outline: none;
    box-shadow: 0 0 0 2px #69f6b7;
  }

  button {
    font-family: "Raleway", sans-serif;
    font-weight: bold;
    height: 30px;
    width: 30px;
    padding-top: 2px;
    background-color: #69f6b7;
    border: none;
    border-radius: 50%;
    cursor: pointer;

    &:hover {
      background-color: #a6f9d4;
    }
  }
`;

export default class NewItemForm extends Component {
  state = {
    newItemName: "",
  };

  handleListSubmit = async (e) => {
    e.preventDefault();
    const { newItemName } = this.state;
    const { listId, setListItems } = this.props;

    if (newItemName) {
      const connection = await fetch(`/lists/${listId}`, {
        method: "POST",
        body: JSON.stringify({ newItemName }),
        headers: { "Content-Type": "application/json" },
      });
      const data = await connection.json();
      setListItems(data.listItems); // update list items
      this.setState({ newItemName: "" }); // reset form
    }
  };

  handleInputChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <FormContainer>
        <form onSubmit={this.handleListSubmit}>
          <input
            type="text"
            name="newItemName"
            value={this.state.newItemName}
            placeholder="Add a new item..."
            onChange={this.handleInputChange}
          />
          <button>
            <PlusIcon />
          </button>
        </form>
      </FormContainer>
    );
  }
}
