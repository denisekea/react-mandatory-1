import React, { Component } from "react";
import styled from "styled-components";
import TodoList from "../../components/Todos/TodoList";
import NewListForm from "../../components/Todos/NewListForm";

const ListOverviewContainer = styled("div")`
  background: linear-gradient(
    346deg,
    rgba(255, 255, 255, 1) 0%,
    rgba(62, 0, 235, 1) 77%
  );
  height: 100%;
  overflow: auto;

  h1 {
    font-family: "Montserrat", sans-serif;
    text-align: center;
    color: white;
    margin: 1em;
  }
`;

const ListsGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 1em;
  max-width: 80vw;
  margin: 3em auto 5em;

  @media (min-width: 1550px) {
    max-width: 65vw;
  }
`;

export default class ListsPage extends Component {
  state = {
    lists: [],
  };

  // list overview
  async componentDidMount() {
    try {
      const response = await fetch("/lists");
      const data = await response.json();
      this.setState({ lists: data.listData });
    } catch (error) {
      console.log(error);
    }
  }

  handleUpdateLists = (listData) => {
    this.setState({ lists: listData });
  };

  render() {
    const { lists } = this.state;

    return (
      <ListOverviewContainer>
        <h1>MY LISTS</h1>
        <NewListForm setLists={this.handleUpdateLists} />
        <ListsGrid>
          {lists &&
            lists.map((list) => {
              return (
                <TodoList
                  key={list.listId}
                  list={list}
                  setLists={this.handleUpdateLists}
                />
              );
            })}
        </ListsGrid>
      </ListOverviewContainer>
    );
  }
}
