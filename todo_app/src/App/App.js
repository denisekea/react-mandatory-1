import React from "react";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import ListsPage from "../routes/Todos/ListsPage";

const AppContainer = styled("div")`
  margin: 0;
  height: 100%;
`;

const App = () => {
  return (
    <Router>
      <AppContainer>
        <Switch>
          <Route exact path="/">
            <Redirect to="/todos" />
          </Route>
          <Route
            path="/todos"
            component={(props) => <ListsPage {...props} />}></Route>
        </Switch>
      </AppContainer>
    </Router>
  );
};

export default App;
